## SMTSR - SMart Test Suite Reduction tool

### Problem - redundant test cases

In growing projects in which new test cases are added alongside each new feature, testing process may significantly prolong over the time. There is a good probability that new tests are perhaps also testing parts of the programme which are already covered by older tests. In order to decrease that effect, the size of the test suite can be reduced by eliminating redundant test cases.

### About this work

This project is a part of *Software Verification* course assignment and as such is a rudimentary attempt to address mentioned issues in testing phase of the software development.

Considerable care must be taken when adding new test cases if one wants to avoid redundancy, so the aim of this tool is to demonstrate how it could be done automatically.

We implement an interesting solution for detecting redundant test cases in the given test suite, while also suggesting one equivalent test suite (by code coverage metric we define). For more details see [this](https://blirena.bitbucket.io/) presentation or read [this](https://bitbucket.org/blirena/vs/src/master/docs/smtsr.pdf) PDF document (in Serbian).

### Installation

#### Install Python 3, including its package-management tool `pip`, `numpy` and `pandas` packages

``` bash
sudo apt-get install python3 python3-pip
pip3 install numpy pandas
```

#### Install Z3 solver

``` bash
git clone --depth=1 https://github.com/Z3Prover/z3.git
cd z3
python3 scripts/mk_make.py
cd build
make
sudo make install
```

#### Install gcc-9

``` bash
sudo add-apt-repository ppa:jonathonf/gcc-9.0
sudo apt-get update
sudo apt-get install gcc-9 g++-9
```

In order to run this project, gcc must be set to version 9. This can be done interactively by running `sudo update-alternatives --config gcc` and choosing version 9. Same needs to be done in case of g++ and gcov.

Another way is the following -- first check output of `update-alternatives --query gcc` (same for g++ and gcov). Note the priorities attributed to alternatives, as gcc-9's priority has to be the highest. Following commands set the priority to 10.

``` bash
sudo update-alternatives --install /usr/bin/gcc gcc $(which gcc-9) 10
sudo update-alternatives --install /usr/bin/g++ g++ $(which g++-9) 10
sudo update-alternatives --install /usr/bin/gcov gcov $(which gcov-9) 10
```

To roll back to system's default version, execute `sudo update-alternatives --remove-all gcc` or `sudo update-alternatives --config gcc` (same for g++ and gcov).

### Example project installation

The instructions for setting up *Geometrijski algoritmi* project, which we used as an example project, can be found [here](EXAMPLE_PROJECT_GTEST.md).

### LLVM project setup

The instructions for building LLVM project can be found [here](LLVM_INSTRUCTIONS.md).
