#!/bin/bash

usage_str="Usage: ./setup_llvm.sh <number_of_threads>"
if [ "$#" -ne 1 ]; then
    echo $usage_str
    exit 1
fi

# Number of threads to use for make jobs
n=$1

sudo_privileges="sudo"
if [[ `whoami` == 'root' ]]; then
    sudo_privileges=""
fi

$sudo_privileges apt-get update
if [[ $? != 0 ]]; then
    echo "Check Internet connection and whether sudo is installed."
    echo "Then install these packages: cmake cmake-curses-gui"
else
    $sudo_privileges apt-get install cmake cmake-curses-gui
fi

git clone --depth=1 https://github.com/llvm-mirror/llvm

cd llvm; mkdir llvm-build; cd llvm-build
cmake -G "Unix Makefiles" -DLLVM_ENABLE_PROJECTS="" -DCMAKE_BUILD_TYPE=Debug ../

echo "Running ccmake..."
echo "Press [t] to uncover advanced flags."
echo "Set BENCHMARK_CXX_FLAGS_COVERAGE, CMAKE_CXX_FLAGS_DEBUG and CMAKE_C_FLAGS_DEBUG flags to:"
echo "-O0 -g --coverage -fprofile-filter-files='.*Optional.h;.*SmallVector.h;.*iterator.h;.*iterator_range.h;.*abi-breaking.h;.*ErrorHandling.h;.*Twine.h;.*DenseMap.h;.*Triple.h;.*CallSite.h;.*Function.h;.*Module.h;.*PassManager.h;.*Pass.h;.*StringExtras.h;.*Instructions.h;.*Intrinsics.h;.*MCTargetOptionsCommandFlags.inc;.*SubtargetFeature.h;.*CodeGen.h;.*CommandLine.h;.*Host.h;.*TargetOptions.h;.*BuiltinGCs.h;.*Passes.h;.*SchedulerRegistry.h;.*MemoryBuffer.h;.*MachineFunction.h;.*ArrayRef.h;.*PointerIntPair.h;.*MCContext.h;.*MCSymbol.h;.*APInt.h;.*StringRef.h;.*PBQPRAConstraint.h;.*ScheduleDAGMutation.h;.*MCSubtargetInfo.h;.*STLExtras.h;.*DerivedTypes.h;.*Type.h;.*Casting.h;.*MathExtras.h;.*Types.h;.*DebugLoc.h;.*CBindingWrapping.h;.*YAMLTraits.h;.*DiagnosticHandler.h;.*Options.h;.*StringMap.h;.*Attributes.h;.*Comdat.h;.*DataLayout.h;.*GlobalAlias.h;.*GlobalIFunc.h;.*GlobalVariable.h;.*Metadata.h;.*ProfileSummary.h;.*SymbolTableListTraits.h;.*DiagnosticInfo.h;.*Error.h;.*raw_ostream.h;.*Regex.h;.*SmallPtrSet.h;.*ManagedStatic.h;.*SmallString.h;.*llvm-config.h;.*Chrono.h;.*ErrorOr.h;.*MD5.h;.*Allocator.h;.*PrettyStackTrace.h;.*CommandLine.h;.*None.h;.*PointerUnion.h;.*SMLoc.h;.*DisassemblerTypes.h;.*FormattedStream.h;.*Pass.h;.*AliasAnalysis.h;.*AssumptionCache.h;.*InlineCost.h;.*ValueHandle.h;.*ValueMapper.h;.*TargetLibraryInfo.h;.*CommandFlags.inc;.*LinkAllAsmWriterComponents.h;.*LinkAllCodegenComponents.h;.*MIRParser.h;.*MachineFunctionPass.h;.*MachineModuleInfo.h;.*TargetPassConfig.h;.*TargetSubtargetInfo.h;.*AutoUpgrade.h;.*DiagnosticPrinter.h;.*IRPrintingPasses.h;.*LLVMContext.h;.*LegacyPassManager.h;.*Verifier.h;.*IRReader.h;.*Debug.h;.*FileSystem.h;.*InitLLVM.h;.*PluginLoader.h;.*SourceMgr.h;.*TargetRegistry.h;.*TargetSelect.h;.*ToolOutputFile.h;.*WithColor.h;.*Cloning.h;.*llc.cpp'"
echo "Press [c] and [g] when done."
ccmake .

echo "Running make..."
make -j$n
