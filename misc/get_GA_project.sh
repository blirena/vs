#!/bin/bash

exit_msg="Please run Geometrijski algoritmi application at least once and build GATest project in Debug mode."

sudo_privileges="sudo"
if [[ `whoami` == 'root' ]]; then
    sudo_privileges=""
fi

$sudo_privileges apt-get update
if [[ $? != 0 ]]; then
    echo "Check Internet connection and whether sudo is installed."
    echo "Then install these packages: build-essential libgl1-mesa-dev freeglut3-dev"
else
    $sudo_privileges apt-get install build-essential libgl1-mesa-dev freeglut3-dev
fi

# Cover case when script is called using source, bash, sh or .
if [[ "$0" == "." || "$0" == "source" || "$0" == "bash" || "$0" == "sh" ]]; then
    script_path=$1
else
    script_path=$0
fi

# Extract misc directory from script invocation string
misc_dir=$(dirname "$0")
if [[ $misc_dir[0] != "/" ]]; then # Relative path
    misc_dir="../$misc_dir"
fi
if [[ $misc_dir == "" ]]; then  # Directly executed
    misc_dir="../"
fi

git clone --depth=1 https://bitbucket.org/geoalg1819/c.git
cd c && git apply $misc_dir/0001-clean.patch
if [[ $? != 0 ]]; then
    echo "Applying patch failed!"
    exit 1
fi
git clone --depth=1 https://github.com/google/googletest

echo "Do you want to install Qt? (You need Qt >= 5.12)"
read -p "[y|n]" installQt

if [[ ! $installQt =~ ^[Yy]$ ]]; then
    echo $exit_msg
    exit 0
fi

wget http://download.qt.io/official_releases/qt/5.12/5.12.3/qt-opensource-linux-x64-5.12.3.run
chmod +x qt-opensource-linux-x64-5.12.3.run
echo "The Qt installer will start in a moment."
echo "Once you get to the \"Select Components\" screen, under \"Qt\" section, choose
	 at least \"Qt 5.12\" (\"Desktop gcc 64-bit\" and \"Qt Charts\" are sufficient)."
./qt-opensource-linux-x64-5.12.3.run

echo $exit_msg
