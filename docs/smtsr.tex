% !TEX encoding = UTF-8 Unicode
% Komanda kompajliranja: pdflatex

\documentclass[a4paper]{article}
\usepackage[total={6in, 8in}]{geometry}

\usepackage[english,serbian]{babel}
\usepackage[unicode]{hyperref}
\hypersetup{colorlinks, citecolor=green, filecolor=green, linkcolor=blue, urlcolor=blue}

\usepackage{url}
\usepackage[utf8]{inputenc}
\usepackage{graphicx, subfigure}
\usepackage{amsmath}
\usepackage{float}

\usepackage{forest}
\usepackage{kbordermatrix}
\newtheorem{definicija}{Definicija}

\usepackage{listings}
\renewcommand{\lstlistingname}{Blok}
\definecolor{blok-boja}{rgb}{0.96, 0.96, 0.96}

\begin{document}

\title{SMTSR\\ \normalsize{Automatsko detektovanje redundantnih testova}\\ \small{Seminarski rad u okviru kursa\\Verifikacija softvera\\ Matematički fakultet}}

\author{Irena Blagojević, Gorana Vučić\\ Stefan Žikić, Nikola Katić}
\maketitle

\abstract
{
  Ovim radom prikazan je jedan pristup detektovanja redundantnih test-primera i dat je opis implementacije i korišćenja. Implementirani alat oslanja se na SMT rešavač koji među svim skupovima neredundantnih testova ekvivalentnih polaznom daje prednost onom koji ima minimalno vreme izvršavanja. Pojam ekvivalentnosti izvršavanja skupa testova postpuno je uveden. Svrha opštijeg opisa problema i koraka tokom izgradnje modela je da se ostavi mogućnost jednostavne promene nekih odluka -- u slučaju usavršavanja trenutnog stanja alata, ili dobijanja novog alata koji bi radio u skladu sa opisanim opštim principima. {\em{LLVM}}-ov alat \texttt{llc} i {\em{C++}} projekti koji koriste {\em{Google}} testove su podržani alatom {\em{SMTSR}}.
}

\tableofcontents
\newpage

\section{Uvod}

Tokom procesa razvoja softvera, uporedo sa dodavanjem novih funkcionalnosti, poželjno je voditi računa i o testiranju istih. Povećanjem skupa testova (eng. {\em{test suite}}) otežava se vođenje evidencije o delovima komponenti koje su već izložene proveri, pa pažljiv odabir novih testova predstavlja težak zadatak. U idealnom slučaju, novi testovi bi trebalo da testiraju minimum k\^ oda koji u potpunosti obuhvata neku novu funkcionalnost, ali i tada postoji mogućnost da se njihovim dodavanjem neki postojeći načine suvišnim. Ukupno vreme izvršavanja predstavlja važnu osobinu skupa testova, pa će centralno pitanje ovog rada biti kako redukovati skup testova, a samim tim i vreme izvršavanja, na način kojim se zadržava što je više moguće sličan skup funkcionalnosti izloženih proveri.

Treba imati u vidu da svrha testova prevazilazi prostu proveru funkcionalnosti aplikacije. Podvrgavanje tehničkih aspekata programa proveri potpada u {\em{nefunkcionalno testiranje}}\cite{predavanja}. Naime, moguće je testirati kako se program ponaša prilikom nepredviđenih i pogrešnih ulaza, ili prilikom velikog opterećenja (eng.~{\em{stress testing}}). U svrhu demonstracije implementirane ideje, nadalje će biti razmatrano funkcionalno testiranje, a za pojedine druge koncepte biće data naznaka rešenja.

\subsection{Postavka problema}

Problem automatskog uklanjanja redundantnih testova iz datog skupa $S$ se može svesti na problem minimalnog pokrivanja skupa (eng.~{\em{set cover problem}}). Znajući da svaki test $t$ izlaže proveri neki skup elemenata $\Phi(t)$, potrebno je odabrati minimalni podskup $P \subseteq S$, takav da je $\bigcup_{t \in P}\Phi(t) = \bigcup_{t \in S}\Phi(t)$. Ovaj problem se nalazi u listi {\em{Karpovih 21 NP-kompletnih problema}}\footnote{Ričard Karp (eng.~{\em{Richard Manning Karp}}), američki naučnik, dobitnik Tjuringove nagrade 1985. godine. U radu iz 1972. godine sastavio je listu od 21-og problema za koje je, svođenjem na SAT, dokazao NP-kompletnost.}.

\begin{definicija} \label{def1}
  Testovi $t_1, t_2 \in S$ su duplikati akko $\Phi(t_1)$ = $\Phi(t_2)$.
\end{definicija}

Pronalaženje i eliminisanje svih duplikata predstavlja osnovnu varijantu problema. Verovatnoća za postojanje duplikata se menja zavisno od definicije skupa $\Phi$, pa to treba uzeti u obzir prilikom definisanja. Kroz više iteracija apstrahovanja modela, dobijen je praktično primenljiv alat, a u nastavku će postupno biti prikazana izgradnja takvog modela.

\section{Izgradnja modela}

Najpre će biti konstruisan model koji je {\em{saglasan}}\footnote{Ukoliko se neki test proglasi redundantnim, onda to zaista i jeste. Ova osobina se takođe prevodi i kao {\em{pouzdanost}.}} (eng.~{\em{sound}}) -- u meri u kojoj to CFG (eng.~{\em{control flow graph}}) omogućuje. Imajući u vidu da su samo pojedine pretpostavke ovog pristupa usvojene u implementaciji, u nastavku će biti dato pojednostavljeno objašnjenje. Glavna pretpostavka je da ukoliko dva testa obilaze istu putanju u CFG-u, onda u redukovanom skupu nije potrebno zadržati oba.

\subsection{CFG-zasnovan model}

Dosadašnjom postavkom problema nije precizirano šta su tačno elementi skupa $\Phi$. U skladu sa gorenavedenom definicijom, za $\Phi(t)$ se može uzeti putanja u CFG-u koja se obilazi pokretanjem testa $t$. Ako se dodatno pretpostavi i numeracija grana u grafu, moguće je sortirati testove leksikografski (po granama), poredeći odgovorajuće putanje. Tako bi se nakon sortiranja, ukoliko postoje duplikati, oni nalazili na susednim pozicijama.

Prethodnom procedurom nisu obuhvaćena ,,više ka više'' poređenja. Slučajevi u kojima je $\Phi(t_1) \cup \Phi(t_2) = \Phi(t_3)$ ne mogu biti prepoznati bez proširenja definicije \ref{def1}, pa je zato neophodno pribeći {\em{nagodbi između saglasnosti i performansi}}\footnote{Na engleskom~{\em{trading soundness for performance}} -- pravljenje ustupaka nauštrb saglasnosti, kako bi se dobio efikasniji sistem. Sintagma koja se u kontekstu konkoličkog izvršavanja koristi u \cite{konkolicko}.}. Umesto da se posmatra putanja na nivou cele aplikacije koja se testira\footnote{Zbog načina na koji se jedinični testovi implementiraju, potrebno je ignorisati čvorove grafa koji odgovaraju k\^ odu samog testnog projekta, ali takvi detalji su zanemareni.}, poželjno bi bilo posmatrati {\em{delove}} k\^ oda (u nastavku teksta -- {\em{jedinice k\^ oda}}). Za svaki test $t$ i jedinicu k\^ oda $f$ uvedimo $\Psi(t, f)$ koja u izvesnom smislu predstavlja restrikciju $\Phi(t)$ na jedinicu k\^ oda $f$. Ako prilikom izvršavanja testa $t$ program više puta ulazi i izlazi iz jedinice k\^ oda $f$, onda je $\Psi(t, f)$ unija odgovarajućih putanja dekartovski pomnožena nekim identifikatorom za $f$.

\begin{definicija} \label{def2}
  Testovi $t_1, ..., t_n \in S_1$ i $t'_1, ..., t'_m \in S_2$ su ekvivalentni u odnosu na jedinicu k\^ oda $f$, u oznaci $S_1 \equiv_f S_2$ akko važi da je $\bigcup_{t \in S_1}\Psi(t, f) = \bigcup_{t \in S_2}\Psi(t, f)$.
\end{definicija}

Neka je sa $\phi(S)$ označen skup svih jedinica k\^ oda kroz koje je tok izvršavanja programa prošao pokretanjem testova iz $S$. Primera radi, to mogu biti svi obiđeni metodi ili fajlovi. Pretpostavkom raspoloživosti prethodno pomenutih skupova, dolazi se do opštije definicije redundantnosti.

\begin{definicija} \label{def3}
  Testovi $t_1, ..., t_n \in S_1$ su redundantni u skupu testova $S \supseteq S_1$ akko $(\forall f \in \phi(S))\\ S \equiv_f S'$, gde je $S'=S\setminus S_1$.
\end{definicija}

Prethodna definicija ne daje naročito intuitivnu predstavu problema, stoga će operativniji način sagledavanja uvedenih pojmova biti dat u nastavku. Definišimo matricu $\mathcal{M}_{i, j} = \Psi(t_i, f_j)$ dimenzija $m\times n$, gde je $n$ broj testova, a $m$ ukupan broj jedinica k\^ oda.
$$
\kbordermatrix{
          &f_1    &f_2     &                 &f_m\cr
  t_1     &\Psi(t_1, f_1)  &\Psi(t_1, f_2)    &\ldots &\Psi(t_1, f_m)\cr
  t_2     &\Psi(t_2, f_1)  &\Psi(t_2, f_2)    &\ldots &\Psi(t_2, f_m)\cr
          &\vdots         &\vdots            &\ddots &\vdots\cr
  t_n     &\Psi(t_n, f_1)  &\Psi(t_n, f_2)    &\ldots &\Psi(t_n, f_m)
}
$$

Matričnim tumačenjem definicije \ref{def1} dobija se jednostavniji kriterijum poređenja dva testa. Testovi $t_i$ i $t_j$ su duplikati ukoliko su $i$-ta i $j$-ta vrsta matrice $\mathcal{M}$ pokoordinatno jednake. Iz \ref{def2}, a u terminima matrice $\mathcal{M}$, definiše se ekvivalentnost dva skupa testova: $S_1 \equiv_f S_2$ akko je unija svih elemenata iz redova koji odgovaraju testovima skupa $S_1$, jednaka uniji svih redova koji odgovaraju testovima skupa $S_2$, posmatrajući samo vrednosti iz kolone koja odgovara $f$. Konačno, definicija \ref{def3} daje kriterijum kojim se iz početnog skupa testova mogu eliminisati redundantni -- potrebno je pronaći one redove čijim se izbacivanjem dobija matrica $\mathcal{M'}$, takva da je $\bigcup\limits_{i,j}\mathcal{M'}_{i, j} = \bigcup\limits_{i,j}\mathcal{M}_{i, j}$.

Ograničenje ovako definisanog modela se u praksi ogleda u složenosti postupka kreiranja grafa\cite{predavanja}, a dodatno se usložnjava računanjem skupa $\Psi(t, f)$ za svako $t$ i sve $f \in \phi(S)$, odnosno kreiranjem matrice za ovako odabrane skupove. U sledećem delu rada biće uvedena metrika kojom se model dodatno udaljava od saglasnosti, sa ciljem poboljšanja efikasnosti.

\subsection{Model zasnovan na pokrivenosti}

Trenutna procedura detektovanja redundantnih testova se svodi na traženje onih redova čijim se izbacivanjem ne menja unija svih elemenata matrice. Opštost uvedenog modela daje mogućnost promene pojedinih odluka, tako da se sam postupak zadrži.

Uvedimo nov način tumačenja $\Phi(t)$ - skupa elemenata koje test $t$ izlaže proveri. Pretpostavka koja se dodaje je da procenat pokrivenosti k\^ oda omogućava razlikovanje elemenata koji su izloženi proveri. Intuitivno, ova pretpostavka za velike projekte ima opravdanja jer kada jedinica k\^ oda ima mnogo linija, verovatnoća da pokrivenost naredbi bude ista, nije velika ukoliko se posmatraju neredundanti testovi. U skladu sa promenom funkcije $\Phi(t)$, i skup $\Psi(t, f)$ dobija drugo značenje.

\begin{definicija}\label{def4}
  Neka procentualna pokrivenost naredbi jedinice k\^ oda $f$ prilikom pokretanja testa $t$ iznosi $c \in [0, 100]$. Funkcija $\Psi(t, f)$ se preslikava u jednočlan skup $\{c, f\}$.
\end{definicija}



Na ovaj način je dvojako izvršena apstrakcija. Identičnom pokrivenošću dela k\^ oda se u obzir ne uzima i redosled izvršavanja tih instrukcija, pa odatle sledi nemogućnost garantovanja iste CFG putanje -- što predstavlja prvi vid pojednostavljenja. Izolovanim posmatranjem jedinice k\^ oda $f$ se na višem nivou uvodi dodatan sloj apstrakcije. Korišćenjem relacije $\equiv_f$ u definiciji \ref{def3} se sakriva informacija o redosledu izvršavanja različitih $f$.
Ukoliko se kao jedinica k\^ oda posmatra {\em{metoda}}, naredna definicija se uklapa u specijalni slučaj definicije \ref{def3} za $n = 1$.

\begin{definicija}\label{def5}
  Test $t$ je redundantan u odnosu na skup testova $S$ akko za svaki metod koji $t$ izvršava, postoji neki drugi test iz skupa $S$ koji ga izvršava na ekvivalentan način\cite{rostra}.
\end{definicija}

Ako se u obzir dodatno uzme i tumačenje zasnovano na pokrivenosti, dobija se operativniji oblik definicije \ref{def5} koji predstavlja centralnu tačku implementirane ideje.

\begin{definicija}\label{def6}
  Test $t$ je redundantan u odnosu na skup testova $S$ akko za svaki metod koji $t$ izvršava, postoji neki drugi test iz skupa $S$ koji ga izvršava sa istim procentom pokrivenosti naredbe.
\end{definicija}

Problem koji se rešava je minimizacione prirode -- potrebno je izvršiti odabir redundantnih testova $t_1$, $t_2$, ..., $t_k$ tako da preostali skup bude minimalne kardinalnosti. Matrično posmatrano, ako za $j$-tu kolonu grupišemo vrste po vrednosti, onda je iz svake grupe neophodno zadržati bar jedan test u redukovanom skupu. Ovaj uslov treba da važi za svaku kolonu matrice $\mathcal{M}$.

Ovako kreiran model predstavlja instancu CSP (eng.~{\em{constraint satisfaction problem}}) vrste problema\cite{rzoltar}. Rešenje za redukovanje skupa testova se sada može dobiti sprovođenjem sledećih suštinskih koraka:
\begin{itemize}
\item[--] {\em{izgradnja matrice $\mathcal{M}$} i grupisanje po kolonama}
\item[--] {\em{konstruisanje uslova za svaku od kolona u SMT2 formatu}}
\item[--] {\em{rešavanje problema korišćenjem SMT rešavača}}
\end{itemize}
Promena u do sad opisanom postupku je odluka da se umesto metoda vrši praćenje pokrivenosti na nivou fajlova. Glavni razlozi za takvu odluku su povećanje jednostavnosti i efikasnosti implementacije. Na ovaj način u matrici $\mathcal{M}$ svaka kolona odgovara jednom fajlu.

\section{Opis arhitekture sistema}

Implementiranjem dobijenih koraka, nastao je alat od svega nekoliko stotina linija {\em{Pyhton}} k\^ oda koji smo nazvali SMTSR (akr.~{\em{Smart Test Suite Reduction}}). Program je sačinjen od 5 skripti, a njihov odnos u vidu UML-dijagrama komponenti, dat je na slici \ref{uml-dijagram}.

Glavna skripta {\em{smtsr.py}} koristi funkcionalnosti ostalih modula. Interakcija korisnika sa alatom se postiže radom iz komandne linije. Korisnički interfejs implementiran je u {\em{cli.py}}.

U zavisnosti od ciljanog tipa projekta, skripta {\em{testing\_utils.py}} pronalazi odgovarajuća imena testova. Nakon što se ti podaci proslede do glavne skripte, odatle se pozivaju metode iz {\em{coverage\_matrix.py}}, kojima se konstruiše matrica pokrivenosti $\mathcal{M}$. Za dobijanje informacija o pokrivenosti koristi se alat {\em{fastcov}}\footnote{Originalna implementacija alata se može naći na \url{https://github.com/RPGillespie6/fastcov}.}. S obzirom da se elementi porede samo na jednakost, svejedno je da li se vodi evidencija o broju pogođenih linija ili procentu pokrivenosti. Ova činjenica je iskorišćena, jer je broj pogođenih linija lakše dostupna informacija.

Nakon što je matrica konstruisana, rad alata se premešta u {\em{reduce.py}}, koja na osnovu matrice generiše uslove u SMT2 formatu, a zatim ih i prosleđuje {\em{z3}} rešavaču\footnote{Link ka adresi sa korisnim informacijama \url{https://rise4fun.com/Z3}.}. Rešavač se poziva kao potproces, a parsiranjem izlaza se dobija model.

Pozivanjem skripte sa {\texttt{/putanja/do/smtsr.py --help}}, dobijaju se korisne informacije o mogućnostima alata. Konkretni primeri korišćenja dati su u narednoj glavi.

\begin{figure}[H]
  \center
  \includegraphics[scale=0.04]{SMTSR_UML_opaque}
  \caption{UML-dijagram komponenti implementiranog sistema.}
  \label{uml-dijagram}
\end{figure}

\section{Korišćenje SMTSR alata}

Alat koji smo implementirali moguće je koristiti za {\em{C++}} projekte koji koriste {\em{Google Test}} biblioteku za testiranje, ali postoji podrška i za {\em{LLVM}}-ov alat \texttt{llc}. Pozivanjem glavne skripte sa argumentom \texttt{-g} ili \texttt{-l}, korisnik se opredeljuje za jedan od ta dva režima rada. Opšti format korišćenja alata je \texttt{/putanja/do/smtsr.py -g|-l <src-dir> <build-dir> <test-exec>}. Korisnički interfejs je implementiran korišćenjem biblioteke \texttt{argparse} i time su dobijeni deskriptivni ispisi kojima se objašnjava korišćenje. Ukoliko korisnik pogreši prilikom navođenja nekog od argumenata, o neispravnosti tog argumenta biće i obavešten.

\subsection{Alati i zavisnosti}
Detaljno uputstvo šta je sve potrebno instalirati od programa da bi {\em{SMTSR}} bilo moguće uspešno pokrenuti, dato je u okviru fajla {\em{INSTALL.md}}. Ukratko, potrebno je imati instaliran {\em{Python 3}} sa {\em{numpy}} i {\em{pandas}} modulima i dostupnom bibliotekom {\em{z3}} rešavača. Kao zavisnost za {\em{fastcov.py}} neophodno je imati i {\em{gcc-9}} verziju kompajlera\footnote{Unapređenje {\em{gcov}} alata mogućnošću prosleđivanja više fajlova odjednom dozvolilo je paralelizaciju generisanja informacija o pokrivenosti.}. U delu koji sledi podrazumevaće se da su sve zavisnosti instalirane.

\subsection{Primeri korišćenja}

U naredna dva potpoglavlja dato je uputstvo korišćenja alata za konkretne projekte. Primena {\em{SMTSR}} alata na projekte koji koriste {\em{Google Test}} biblioteku biće prikazana na primeru jednog studentskog projekta.

Treba napomenuti da nakon što je uočeno da se dobijaju matrice velikih dimenzija, iz razmatranja prilikom posmatranja pokrivenosti izbačena su sistemska zaglavlja. Iskorišćena je nova opcija \texttt{-fprofile-filter-files}, uvedena u {\em{gcc-9}}, koja omogućuje izolovano posmatranje pokrivenosti samo za prosleđene fajlove. Osvrt na ovo biće dat u delu gde je prikazan rezultat pokretanja alata za {\em{llc}}.

\subsubsection{Google testovi}
Uputstvo kako instalirati i osposobiti jedan studentski projekat za korišćenje našeg alata, sadržano je u {\em{EXAMPLE\_PROJECT\_GTEST.md}} datoteci. S obzirom na detaljnost opisanog postupka, nadalje će biti podrazumevana uspešnost instalacije, kao i hijerarhija sa slike \ref{hijerarhija}.

\begin{figure}
  \centering
  \scalebox{0.6}{
  \begin{forest}
    for tree={
      font=\ttfamily,
      grow'=0,
      child anchor=west,
      parent anchor=south,
      anchor=west,
      calign=first,
      edge path={
        \noexpand\path [draw, \forestoption{edge}]
        (!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
      },
      before typesetting nodes={
        if n=1
        {insert before={[,phantom]}}
        {}
      },
      fit=band,
      before computing xy={l=15pt},
    }
    [[c
      [build-GATest-Desktop]
      [build-Geometrijski\_Algoritmi]
      [GATest]
      [Geometrijski\_Algoritmi]
      ]
     [vs
      [docs]
      [misc]
      [presentation]
      [scripts]
      ]
    ]
  \end{forest}
  }
  \caption{Prikaz dva nivoa željene hijerarhije direktorijuma.}
  \label{hijerarhija}
\end{figure}

Problem sa kojim smo se susreli tokom primene alata na datom projektu je nedeterminizam za koji je ustanovljeno da potiče od testova koji u sebi sadrže nasumično generisanje podataka\footnote{Takva vrsta testiranja se naziva {\em{fuzz-testing}} ili {\em{monkey-testing}}. Istorijske reference ovih pojmova \cite{fuzz, monkey-lives}.}. Za takve testove, prilikom različitih pokretanja alata, nije zagarantovana ista pokrivenost, pa samim tim ni ista matrica $\mathcal{M}$, što nas onemogućava da jednoznačno kreiramo SMT2 uslove. Primenom {\em{0001-clean.patch}} izmena, takvi testovi su izuzeti iz razmatranja.

Nakon pozicioniranja u {\em{vs}} direktorijum, potrebno je pokrenuti komandu iz bloka \ref{ulaz-google}, čime bi trebalo da se dobije izlaz dat u bloku \ref{izlaz-google}. Kreiranje SMT2 fajla koji odgovara konkretnom skupu testova se postiže dodavanjem opcionog argumenta \texttt{--export-smt2=<ime-fajla>}.

Dodatno je moguće navesti \texttt{--debug} i \texttt{--binary-mode} opcije. Korišćenjem prve se olakšava uvid u skup svih testova time što se na standardni izlaz ispisuje i matrica $\mathcal{M}$, a testovima se pojednostavljuje ime. Ova opcija je prvenstveno namenjena olakšavanju procesa debagovanja. Drugu opciju ima smisla navoditi kada se želi postići dobijanje drastičnijeg redukovanja početnog skupa, po cenu preciznosti. Tada se kreiranje SMT2 uslova vrši na osnovu binarno posmatrane matrice pokrivenosti. U slučaju generisanja nasumičnih podataka, procenat pokrivenosti ne mora uvek biti isti, pa binaran podatak o pokrivenosti može biti bolji odabir.

\begin{lstlisting}[label={ulaz-google}, caption={Primer pokretanje alata za {\em{Google Tests}} režim rada.}, captionpos=b, basicstyle=\footnotesize, backgroundcolor = \color{blok-boja}]
./scripts/smtsr.py -g \
../c/Geometrijski_Algoritmi/algoritmi_studentski_projekti/ \
../c/build-GATest/ \
GATest
\end{lstlisting}

\begin{lstlisting}[label={izlaz-google}, caption={Izlaz nakon poziva komandi iz bloka \ref{ulaz-google}. Liste testova nisu ispisane u celosti.}, captionpos=b, basicstyle=\footnotesize, backgroundcolor = \color{blok-boja}]
starting script with following parameters ....

source_dir:  ../c/Geometrijski_Algoritmi/algoritmi_studentski_projekti
build-dir:  ../c/build-GATest
test-exec-name:  GATest

These are redundant test cases:
['ga10_trilateracija.file2TestLat', 'ga10_trilateracija.file1TestLon' ...

Reduced test-suite proposal:
['ga08_hertelmehlhorn.fileTest', 'ga08_hertelmehlhorn.convexPolygonTest'...

Time reduced: 168ms
Proposed test-suite execution time: 174ms
\end{lstlisting}



\subsubsection{LLVM-ov alat \texttt{llc}}

Uputstvo kako osposobiti {\em{LLVM}} projekat za korišćenje našeg alata, sadržano je u datoteci {\em{LLVM\_INSTRUCTIONS.md}}. Nadalje će biti podrazumevana uspešnost instalacije. Za pokretanje SMTSR alata nad \texttt{llc}-om, neophodno je navesti \texttt{-l} opciju, a zatim i ciljani folder u kojem se nalazi skup testova koji se izlaže proveri. Sledeći argument je putanja do direktorijuma u kojem je {\em{LLVM}} bildovan. Na kraju, potrebno je proslediti {\em{putanju}} do {\em{llvm-lit}} skripte kojom se izvršavaju testovi. U bloku \ref{ulaz-llc}, dat je primer pokretanja za testove iz foldera SPARC, a u bloku \ref{izlaz-llc} odgovarajući izlaz.

Da bi se dobili ovakvi rezultati, instrumentalizovani su samo određeni fajlovi. Njihova lista je data u {\em{misc/fprofile-filter-paths}} fajlu. Nakon isprobavanja više strategija, odabrano je da se instrumentalizuju dva nivoa zaglavlja počevši od {\em{llc.cpp}} fajla.

Naime, ukoliko se instrumentalizuje ceo {\em{llvm}} folder, tada nakon pokretanja {\em{.ll}} testa nastane blizu 1500 {\em{.gcda}} fajlova. Mnogi od tih fajlova nisu relevantni jer imaju identičnu, ili skoro identičnu pokrivenost za ceo posmatrani direktorijum testova. Iz praktičnih razloga smanjenja vremena obrade podataka, bilo bi dobro razmatrati samo najbitnije {\em{.gcda}} fajlove. Stoga smo se opredelili da to budu pomenuta dva nivoa zaglavlja.

\begin{lstlisting}[label={ulaz-llc}, caption={Primer pokretanje alata za {\texttt{llc}} režim rada na SPARC testovima.}, captionpos=b, basicstyle=\footnotesize, backgroundcolor = \color{blok-boja}]
./scripts/smtsr.py -l \
../llvm-repo/llvm/test/CodeGen/SPARC \
../llvm-repo/llvm/llvm-build \
../llvm-repo/llvm/llvm-build/bin/llvm-lit
\end{lstlisting}


\begin{lstlisting}[label={izlaz-llc}, caption={Reformatirani prikaz izlaza nakon poziva komandi iz bloka \ref{ulaz-llc}.}, captionpos=b, basicstyle=\footnotesize, backgroundcolor = \color{blok-boja}]
starting script with following parameters ....

source_dir:  ../llvm-repo/llvm/test/CodeGen/SPARC
build-dir:  ../llvm-repo/llvm/llvm-build
test-exec-name:  ../llvm-repo/llvm/llvm-build/bin/llvm-lit


0  ...CodeGen/SPARC/2006-01-22-BitConvertLegalize.ll is running
1  ...CodeGen/SPARC/2007-05-09-JumpTables.ll is running
...
82 ... CodeGen/SPARC/zerostructcall.ll is running

These are redundant test cases:
['../llvm-repo/llvm/test/CodeGen/SPARC/readcycle.ll',
  '../llvm-repo/llvm/test/CodeGen/SPARC/2013-05-17-CallFrame.ll',
  '../llvm-repo/llvm/test/CodeGen/SPARC/2007-07-05-LiveIntervalAssert.ll']


Reduced test-suite proposal:
['../llvm-repo/llvm/test/CodeGen/SPARC/zerostructcall.ll',
  '../llvm-repo/llvm/test/CodeGen/SPARC/vector-extract-elt.ll',
  '../llvm-repo/llvm/test/CodeGen/SPARC/vector-call.ll' ...

Time reduced: 360ms
Proposed test-suite execution time: 17820ms
\end{lstlisting}


\section{Srodni radovi i dalja unapređenja}

Srž ovog rada čini primena rešavača na klauze dobijene posmatranjem matrice pokrivenosti. Pomenuta ideja preuzeta je iz \cite{rzoltar} i \cite{sat}. Deo koji nije razmatran, a predstavljao bi unapređenje {\em{SMTSR}} alata je izolovano posmatranje testova na osnovu njihovog tipa. Uspešnost izvršavanja testova bi se mogla evidentirati dodatnim kolona-vektorom greške. Test nije uspeo da se izvrši uspešno akko odgovarajući red u vektoru greške iznosi 1. Klauze se dobijaju na isti način, ovoga puta posebno posmatrajući svaku grupu testova.

Pristupi prikazani u \cite{rzoltar, sat} ograničavaju se na posmatranje binarne matrice. Slobodnije tumačenje mere za testiranje je originalni doprinos ovog rada. Odabrana mera je {\em{broj}} pokrivenih naredbi datoteke, čime je sistem nešto bliži saglasnosti. Osim podatka o celobrojnom procentu, autori su ispitavali i mogućnost korišćenja dodatnog podatka koji bi davao precizniju meru. Ukoliko bismo definisali $\Psi(t_i, f_i)$ tako da sadrži informaciju o invarijanti k\^ oda $f_i$ prlikom pokretanja testa $t_i$, tada bi svaka klauza za kolonu $f_i$ imala različitu invarijantu i time bi svako ,,različito'' izvršavanje jedinice k\^ oda $f_i$ bilo sačuvano u redukovanom skupu testova. Jedini dostupan alat za pronalaženje invarijanti koji su autori pronašli zove se {\em{Daikon}}. Izlaz koji se tim alatom dobija (eng.~{\em{operational abstraction}}) je veoma opširan, pa se iz razloga komplikovane implementacije poređenja izlaza od korišćenja {\em{Daikon}} alata odustalo. Rad \cite{operational} nudi induktivan pristup rešavanju problema redukovanja skupa testova korišćenjem operacione apstrakcije.

Generisanje jednog reda matrice pokrivenosti, za neki ciljani test, a zajedno sa mogućnošću čuvanja i učitavanja matrice, predstavljalo bi korisno novo svojstvo {\em{SMTSR}} alata. Tako bi za projekte kod kojih je broj testova i broj fajlova veliki ponovno pokretanje bilo vremenski kraće. Ukoliko na projektu nema promena izvornih fajlova, umesto da se nakon dodavanja novog testa cela matrica generiše iznova, dovoljno je kreirati i dodati samo jedan novi red.

Jednostavna arhitektura alata omogućava proširivost i na drugi tip testova. Sve što je potrebno obezbediti je popunjena matrica. Kolone takve matrice nisu zavisne od tipa jedinice k\^ oda, pa se proširenjem može konstruisati matrica kojom se posmatraju metode, klase ili komponente. Element matrice je takođe podložan promeni, sve dok je numerički podatak i ima smisla kao mera različitosti.

\addcontentsline{toc}{section}{Literatura}
\appendix
\bibliography{seminarski}
\bibliographystyle{plain}

\end{document}
