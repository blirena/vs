#!/usr/bin/env python

import os
import re
import subprocess as sub
import sys

def run_subprocess(command):

    process = sub.Popen(command.split(), stdout = sub.PIPE)
    output, error = process.communicate()
    process.stdout.close()
    process.wait()
    output = output.decode("utf-8")
    return output

def list_of_test_files_llc(path_to_llvm):
    path = path_to_llvm
    files = os.listdir(path)
    list_of_llc_tests = []

    for elem in files:
        elem = path + elem
        if os.path.isdir(elem) :
            list_of_llc_tests = list_of_llc_tests + list_of_test_files_llc(elem + "/")
        elif elem.endswith(".ll") or elem.endswith(".mir") or elem.endswith(".test"):
            output = run_subprocess("cat " + elem)
            found = output.find(" RUN: llc")
            if(found != 0):
                list_of_llc_tests.append(elem)

    return list_of_llc_tests

def list_of_test_names_gtest(test_executable):

    LIST_TESTS_FLAG = 'gtest_list_tests'

    bash_cmd = test_executable + ' --'  + LIST_TESTS_FLAG

    process = sub.Popen(bash_cmd, stdout=sub.PIPE, shell=True)
    process.wait()

    test_suite = ''
    test_names = []

    for line in process.stdout:

        line = line[:-1].decode("utf-8")

        if re.search("\.$", line):
            test_suite = line
        else:
            test_name = re.search("^\s", line)
            if test_name:
                test_names.append(test_suite + test_name.string.strip())

    return test_names

def delete_all_ext_in_path(path_to_instrumented_build,name):

    find_command = ["find", path_to_instrumented_build, "-iname", name]
    xargs_command = ["xargs", "rm","-f"]
    find_process = sub.Popen(find_command, stdout=sub.PIPE)
    xargs_process = sub.Popen(xargs_command, stdout=sub.PIPE, stdin=find_process.stdout)
    xargs_process.stdout.close()
    xargs_process.wait()
