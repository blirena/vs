import numpy as np
from shutil import which
from os.path import isfile
import subprocess

out_file_path = "/tmp/out.smt2"

def set_test_names(test_names_list):
    global T, tNum
    T = test_names_list
    tNum = len(T)

    t_order = {}
    for i in range(tNum):
        t_order[T[i]] = i


def set_file_names(file_names_list):
    global F, fNum
    F = file_names_list
    fNum = len(F)


def set_coverage_matrix(coverage_matrix, times):
    global M, test_time
    M = coverage_matrix
    test_time = times


def generate_clauses(binary_metrics=False):
    """ By default, stricter metric is considered. """

    assertions = ''
    for i in range(fNum):
        # partition column by percentage (i.e. populate "groups" dictionary)
        groups = {}
        for j in range(tNum):
            cov_percent = M[j, i]
            if cov_percent > 0:
                if binary_metrics:
                    cov_percent = 1
                if not cov_percent in groups:
                    groups[cov_percent] = [T[j]]
                else:
                    groups[cov_percent].append(T[j])

        # for each partition (i.e. different percentage value) create clause
        conjuncts = ''
        for perc in groups:
            tests = groups[perc]
            disjuncts = ''
            for t in tests:
                disjuncts += '(> {} 0) '.format(t)
            if len(tests) > 1:
                conjuncts += '(or {}) '.format(disjuncts)
            else:
                conjuncts += '{} '.format(disjuncts)
        if conjuncts:
            alist = '(and {})' if len(groups) > 1 else '{}'
            assertions += '(assert {})\n'.format(alist).format(conjuncts)

    return assertions


def generate_smt2_string(binary_metrics=False):

    # 1) declarations --> integer z3 variables
    decls = ''
    decl = '(declare-fun {} () Int)\n'

    # 2) non_neg --> non-negativity constraints
    non_neg = ' (>= {} 0)'
    non_negs = '(assert (and'
    bool_val = ' (<= {} 1)'
    bool_vals = '(assert (and'

    # 3) clause --> constraint per column (i.e. per file)
    assertions = generate_clauses(binary_metrics=binary_metrics)

    # 4) minimize --> minimize total sum (i.e. get minimal test-suite)

    minimize = ''
    for i in range(0, len(T)):
        decls += decl.format(T[i])
        non_negs += non_neg.format(T[i])
        bool_vals += bool_val.format(T[i])
        minimize += ' (* {} {})'.format(T[i], test_time[i])
    bool_vals += '))\n'
    non_negs += '))\n'

    # 5) check SAT and get model
    start_solver = "(check-sat)\n(get-model)\n"

    # Concatenate all blocks into one string
    smt2_string = decls + "\n" + non_negs + bool_vals + "\n" + assertions + "\n"
    minimize = '(minimize (+{}))\n'.format(minimize)
    smt2_string += minimize + "\n"
    smt2_string += start_solver

    return smt2_string


def generate_smt2lib_file(file_name, binary_metrics=False):

    if not file_name.endswith(".smt2"):
        file_name += ".smt2"
    smt2_string = generate_smt2_string(binary_metrics=binary_metrics)
    with open(file_name, 'w') as file:
        file.write(smt2_string)

def minimize_test_suite():

    if not isfile(out_file_path):
        raise Exception('FAILURE: %s not generated!' % out_file_path)

    if which("z3") is None:
        raise Exception('FAILURE: z3 dependency not met!')

    process = subprocess.Popen(["z3", out_file_path], stdout=subprocess.PIPE)

    test_name = ''
    redundant = []
    reducted_suite = []

    for line in process.stdout:
        line = line[:-1].decode("utf-8")
        if line.endswith('() Int'):
            test_name = line.split(' ')[3]
        elif line.endswith('1)'):
            reducted_suite.append(test_name)
        elif line.endswith('0)'):
            redundant.append(test_name)

    return [redundant, reducted_suite]
