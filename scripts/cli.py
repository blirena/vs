import sys, argparse
from pathlib import Path
import numpy as np
import testing_utils as tu

debug_enabled = False
binary_mode = False
export_file = ""

# font style customization
red_color = "\033[0;31m"
green_color = "\033[0;32m"
bold_font = "\033[1m"
regular_font = "\033[0m"
blue_background = "\033[44m"

# file paths
source_dir = ""
build_dir = ""
test_executable = ""
tests = np.zeros(0)

# usage string
usage_str = "/path/to/smtsr.py -g|-l <src-dir> <test-dir> <test-exec>"
usage_str = bold_font + usage_str + regular_font
usage_msg = "Incorrect usage! Please run script in the following format:\n"
usage_msg = usage_msg + usage_str


def print_tests(test_set, msg, color):
    print ('\n{}{}:\n{}{}{}{}\n'.format(
        bold_font, msg, regular_font, color, test_set, regular_font))

def create_parser():

    # format messages neatly
    description = "Tool for automatic detection of redundant test cases in a test suite.\n"

    title = "SMart Test Suite Reduction"
    blank = " " * ((len(description) - len(title))//2)
    title = blank + blue_background + title + regular_font + "\n"

    border = "-" * len(description) + "\n"
    description = border + title + description + border

    epilog = "{}\n{}EXAMPLE:{}\n".format(border, bold_font, regular_font)
    epilog += "\n- if source files of target project are located in:\n"
    epilog += "'../c/Geometrijski_Algoritmi/algoritmi_studentski_projekti'\n"
    epilog += "\n- if google tests for that project are built in this directory:\n"
    epilog += "../c/build-GATest-Desktop\n"
    epilog += "\n- if google test executable is called GATest\n\n"
    epilog += "Then you may use SMTSR tool by running the following from within vs/:\n\n"
    epilog += "./scripts/smtsr.py -g ../c/Geometrijski_Algoritmi/algoritmi_studentski"
    epilog += "_projekti/ ../c/build-GATest/ GATest"

    # create parser with all relevant messages
    parser = argparse.ArgumentParser(description=description,
                                     usage=usage_str,
                                     epilog=epilog,
                                     formatter_class=argparse.RawTextHelpFormatter)
    return parser


def parse_arguments():
    parser = create_parser()
    parser._action_groups.pop()

    # add command line arguments
    required = parser.add_argument_group('testing type arguments')
    required.add_argument("-g", "--gtest", action="store_true",
                       help="specifying that a project uses Google tests")
    required.add_argument("-l", "--llc", action="store_true",
                       help="specifying that target project is llc")

    pathargs = parser.add_argument_group('path to target files')
    pathargs.add_argument("src_dir", type=Path, help="directory with source files")
    pathargs.add_argument("test_dir", type=Path, help="directory where test files are built")
    pathargs.add_argument("test_exec", type=Path, help="name of the test executable/script")

    optional = parser.add_argument_group("optional arguments")
    optional.add_argument("-d", "--debug", action="store_true",
                          help="use simple test notation t1, t2, ..., tn and \nshow coverage matrix")
    optional.add_argument("-b", "--binary-mode", action="store_true",
                          help="use 0/1 metrics when transforming matrix into smt2")
    optional.add_argument("-f", "--export-smt2", nargs='?', const='out.smt2',
                          help="export to file in smt2 format with the given name")

    return parser.parse_args()


def process_args():
    args = parse_arguments()

    global binary_mode
    binary_mode = args.binary_mode
    global debug_enabled
    debug_enabled = args.debug
    global export_file
    if args.export_smt2:
        export_file = args.export_smt2

    if not args.gtest and not args.llc:
        print(usage_msg)
        exit(64)

    global source_dir
    global build_dir
    global test_executable
    global tests_type
    global tests
    source_dir = str(args.src_dir)
    build_dir = str(args.test_dir)
    test_executable = str(args.test_exec)
    print("starting script with following parameters .... \n")
    print("source_dir: ", source_dir)
    print("build-dir: ", build_dir)
    print("test-exec-name: ", test_executable, "\n\n")

    if args.gtest:
        tests_type = "gtest"
        tests = tu.list_of_test_names_gtest(build_dir + "/" + test_executable)
    else:
        tests_type = "llc"
        tests = tu.list_of_test_files_llc(source_dir+"/")
        tests.sort()
        tu.delete_all_ext_in_path(build_dir+"/","*.gcda")
