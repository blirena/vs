import numpy as np
import pandas as pd
import os
import re
import subprocess as sub
import testing_utils as tu

def print_matrix_in_csv_output(matrx, outputFile):

    dataframe = pd.DataFrame(data = matrx.astype(int))
    dataframe.to_csv(outputFile, sep = ' ', header = False, index=False)

def output_and_time(executive_program, name_of_test, tests_type):

    if tests_type == "llc":
        command = executive_program + " " + name_of_test
        output = tu.run_subprocess(command)
        time = re.findall(" [0-9]*.[0-9]*s\n", output)

        if len(time) > 0:
            time = int((float(re.findall("[0-9]*.[0-9]*", time[-1])[1]))*1000)
        else:
            raise Exception('FAIL: Please report this to developers!')

        return output, time

    else:
        command = "./" + executive_program + " " + "--gtest_filter=" + name_of_test
        output = tu.run_subprocess(command)

        time = re.findall("\([0-9]* ms total\)", output)
        if len(time) > 0:
            time = int(re.findall("[0-9]*", time[-1])[1])
        else:
            raise Exception('FAIL: Please report this to developers!')

        return output, time

def delete_unnecessary_ext(path_to_curr_dir, path_to_init_dir):
    tu.delete_all_ext_in_path(path_to_curr_dir, "*.gcda")
    tu.delete_all_ext_in_path(path_to_curr_dir, "*.gcov")
    tu.delete_all_ext_in_path(path_to_init_dir, "*.info")

def coverage_matrix(list_of_tests, path_to_build_folder, executive_program, tests_type):

    number_of_tests = len(list_of_tests)
    coverage = np.zeros((number_of_tests, 0), dtype = 'i')
    tests_time = np.zeros(number_of_tests, dtype = 'i')
    init_dir = os.path.abspath(os.curdir)
    coverage_map = {}
    files = []

    os.chdir(path_to_build_folder)

    i = 0
    j = 0

    for i in range(number_of_tests):

        delete_unnecessary_ext(os.curdir, init_dir)

        output, tests_time[i] = output_and_time(executive_program, list_of_tests[i], tests_type)

        print(str(i) + " " + list_of_tests[i] + " is running")

        path_to_fastcov = init_dir + "/fastcov.py"
        path_to_report_info = init_dir + "/report.info"
        command_for_fastcov = "python3 " + path_to_fastcov + " -d " + "."  + " --lcov -o " + path_to_report_info
        output = tu.run_subprocess(command_for_fastcov)

        command_for_report = "cat " + path_to_report_info
        task = sub.Popen(command_for_report.split(),stdout=sub.PIPE)
        name_of_file = ""

        for line in task.stdout:
            line = str(line)

            if "SF:" in line:
                start = line.rfind("/")
                end = line.rfind("\\n")
                name_of_file = line[start+1 : end]

                if name_of_file not in coverage_map.keys():
                    coverage_map[name_of_file] = j
                    j = j+1
                    n,m = coverage.shape
                    empty_array = np.zeros((n,1))
                    coverage = np.hstack((coverage,empty_array))

            elif "LH" in line:
                line_coverage = re.search(r'LH:[0-9]*', line)
                line_coverage = int(line[line_coverage.start()+3:line_coverage.end()])
                position = coverage_map[name_of_file]
                coverage[i][position] = line_coverage

        task.wait()

    for key in coverage_map.keys():
        files.append(key)

    os.chdir(init_dir)
    return [coverage, tests_time,files]
