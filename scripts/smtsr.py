#!/usr/bin/env python3
""" SMTSR's main script """

import sys, cli
import coverage_matrix as cm
from shutil import copyfile
from reduce import *
import subprocess as sub

def main(argv):
    cli.process_args()

    M, test_time, files = cm.coverage_matrix(cli.tests, cli.build_dir, cli.test_executable, cli.tests_type)

    tnum = M.shape[0]
    fnum = M.shape[1]

    i = 0

    if cli.debug_enabled:
        print(M)
        cm.print_matrix_in_csv_output(M, "matrica.csv")

        # more readable test and file names
        T = ['t{}'.format(i) for i in range(tnum)]
        F = ['f{}'.format(i) for i in range(fnum)]
    else:
        T = cli.tests
        F = files

    time_by_name = {}
    for i, t in enumerate(T):
        time_by_name[t] = test_time[i]

    set_test_names(T)
    set_file_names(F)
    set_coverage_matrix(M, test_time)

    generate_smt2lib_file(out_file_path, binary_metrics=cli.binary_mode)

    x, y = minimize_test_suite()

    reduced_time = 0
    for eliminated_test in x:
        reduced_time += time_by_name[str(eliminated_test)]
    time_needed = 0
    for left_test in y:
        time_needed += time_by_name[str(left_test)]

    cli.print_tests(x, "These are redundant test cases", cli.red_color)
    cli.print_tests(y, "Reduced test-suite proposal", cli.green_color)

    print("Time reduced: {}{}ms{}\nProposed test-suite execution time: {}{}ms{}\n"\
          .format(cli.bold_font, reduced_time, cli.regular_font,\
                  cli.bold_font, time_needed, cli.regular_font))

    if cli.export_file:
        file_name = cli.export_file
        if not file_name.endswith(".smt2"):
            file_name += ".smt2"
        copyfile(out_file_path, file_name)

if __name__ == "__main__":
    print() # separate script output from command itself
    main(sys.argv[1:])
