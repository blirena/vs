## LLVM project setup (Linux)

Here we will explain how to configure [LLVM](https://llvm.org/) in order to run SMTSR and find redundant test cases for llc.

### Convenience script

You can download it [here](misc/setup_llvm.sh).

### Step-by-step guide

#### Requirements

Except for the requirments for SMTSR, you need to be able to run _CMake_ and its curses interface (`ccmake`).

``` bash
sudo apt-get install cmake cmake-curses-gui
```

#### Building LLVM

First you need to download LLVM to your local machine.

``` bash
git clone --depth=1 https://github.com/llvm-mirror/llvm
```

Once LLVM is cloned, create a build directory inside of it - e.g. `cd llvm & mkdir llvm-build`.
Now you can run `cmake` _from that build directory_ using the following command:

``` bash
cmake -G "Unix Makefiles" -DLLVM_ENABLE_PROJECTS="" -DCMAKE_BUILD_TYPE=Debug ../
```

Next step is to run `ccmake` from the build directory and set flags which will enable instrumentation of the code.

``` bash
ccmake .
```

Curses interface to CMake will open in your terminal. Press `t` to toggle advanced mode. There are three flags that should be altered -- `BENCHMARK_CXX_FLAGS_COVERAGE`, `CMAKE_CXX_FLAGS_DEBUG` and `CMAKE_C_FLAGS_DEBUG`. All three flags should have the same value:

``` bash
-O0 -g --coverage -fprofile-filter-files='.*Optional.h;.*SmallVector.h;.*iterator.h;.*iterator_range.h;.*abi-breaking.h;.*ErrorHandling.h;.*Twine.h;.*DenseMap.h;.*Triple.h;.*CallSite.h;.*Function.h;.*Module.h;.*PassManager.h;.*Pass.h;.*StringExtras.h;.*Instructions.h;.*Intrinsics.h;.*MCTargetOptionsCommandFlags.inc;.*SubtargetFeature.h;.*CodeGen.h;.*CommandLine.h;.*Host.h;.*TargetOptions.h;.*BuiltinGCs.h;.*Passes.h;.*SchedulerRegistry.h;.*MemoryBuffer.h;.*MachineFunction.h;.*ArrayRef.h;.*PointerIntPair.h;.*MCContext.h;.*MCSymbol.h;.*APInt.h;.*StringRef.h;.*PBQPRAConstraint.h;.*ScheduleDAGMutation.h;.*MCSubtargetInfo.h;.*STLExtras.h;.*DerivedTypes.h;.*Type.h;.*Casting.h;.*MathExtras.h;.*Types.h;.*DebugLoc.h;.*CBindingWrapping.h;.*YAMLTraits.h;.*DiagnosticHandler.h;.*Options.h;.*StringMap.h;.*Attributes.h;.*Comdat.h;.*DataLayout.h;.*GlobalAlias.h;.*GlobalIFunc.h;.*GlobalVariable.h;.*Metadata.h;.*ProfileSummary.h;.*SymbolTableListTraits.h;.*DiagnosticInfo.h;.*Error.h;.*raw_ostream.h;.*Regex.h;.*SmallPtrSet.h;.*ManagedStatic.h;.*SmallString.h;.*llvm-config.h;.*Chrono.h;.*ErrorOr.h;.*MD5.h;.*Allocator.h;.*PrettyStackTrace.h;.*CommandLine.h;.*None.h;.*PointerUnion.h;.*SMLoc.h;.*DisassemblerTypes.h;.*FormattedStream.h;.*Pass.h;.*AliasAnalysis.h;.*AssumptionCache.h;.*InlineCost.h;.*ValueHandle.h;.*ValueMapper.h;.*TargetLibraryInfo.h;.*CommandFlags.inc;.*LinkAllAsmWriterComponents.h;.*LinkAllCodegenComponents.h;.*MIRParser.h;.*MachineFunctionPass.h;.*MachineModuleInfo.h;.*TargetPassConfig.h;.*TargetSubtargetInfo.h;.*AutoUpgrade.h;.*DiagnosticPrinter.h;.*IRPrintingPasses.h;.*LLVMContext.h;.*LegacyPassManager.h;.*Verifier.h;.*IRReader.h;.*Debug.h;.*FileSystem.h;.*InitLLVM.h;.*PluginLoader.h;.*SourceMgr.h;.*TargetRegistry.h;.*TargetSelect.h;.*ToolOutputFile.h;.*WithColor.h;.*Cloning.h;.*llc.cpp'
```

This will turn off optimizations, turn on debugging information and code instrumentation. Also, by using `-fprofile-filter-files` option, we can specify a list of source files which should be instrumented. Here we choose to instrument `llc.cpp` file and two levels of header files it includes.

Once these flags are set, press `c` (then `e` if CMake produced some output) and `g`.

Finally, run `make`. Once the build is complete, you can run SMTSR, as explained in [PDF](docs/smtsr.pdf).
