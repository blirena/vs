## Example project installation (Linux)

The project we used as an example project during implementation and evaluation can be found [here](https://bitbucket.org/geoalg1819/c).
It's a project from a master's course on computational geometry (srb. Geometrjski algoritmi).
There are two subprojects - one contains implementation of multiple algorithms (*Geometrijski_Algoritmi*),
and the other contains Google tests for each implemented algorithm (*GATest*).

### Convenience script

You can download it [here](misc/get_GA_project.sh). The following extract from the script supposes it's executed from the `misc` directory, but the script itself can be called from whichever directory.

    ``` bash
    sudo apt-get install build-essential libgl1-mesa-dev freeglut3-dev

    git clone --depth=1 https://bitbucket.org/geoalg1819/c.git
    cd c && git apply ../0001-clean.patch
    git clone --depth=1 https://github.com/google/googletest

    wget http://download.qt.io/official_releases/qt/5.12/5.12.3/qt-opensource-linux-x64-5.12.3.run
    chmod +x qt-opensource-linux-x64-5.12.3.run
    echo "The Qt installer will start in a moment."
    echo "Once you get to the \"Select Components\" screen, under \"Qt\" section, choose
          at least \"Qt 5.12\" (\"Desktop gcc 64-bit\" is sufficient)."
    ./qt-opensource-linux-x64-5.12.3.run
    ```

### Step-by-step guide

Instead of running the given script, you can also follow these steps to setup the Geometrijski algoritmi project.

#### Geometrijski algoritmi project

This is a Bitbucket repository. Clone it using:

`git clone https://bitbucket.org/geoalg1819/c.git`

and a directory "`c`" will show up in the current directory.
There will be two directories inside of it - `GATest` and `Geometrijski_Algoritmi`.

#### Clean project and apply patch

Position yourself in `c` directory and apply patch using `git apply /path/to/misc/0001-clean.patch`.

#### OpenGL

You can install OpenGL by running the following command `sudo apt-get install freeglut3-dev`.

#### Google test

Position yourself in the `c` directory of Geometrijski algoritmi project and clone Google test GitHub repository there.

`git clone https://github.com/google/googletest`

#### Qt >= 5.12

Install the basic requirements for Qt:

`sudo apt-get install build-essential libgl1-mesa-dev`

To download the Qt installer, run the following command:

`wget http://download.qt.io/official_releases/qt/5.12/5.12.3/qt-opensource-linux-x64-5.12.3.run`

Run the installer:

`chmod +x qt-opensource-linux-x64-5.12.3.run ./qt-opensource-linux-x64-5.12.3.run`

Follow the instructions. Once you get to the "*Select Components*" screen, under "*Qt*" section, choose
at least *"Qt 5.12.3"* ("*Desktop gcc 64-bit*" and "*Qt Charts*" are sufficient).

## Running the application

In order to generate `.gcda` files, the application has to be ran at least once.

Start Qt Creator, and add *Geometrijski algoritmi* project by opening its `Geometrijski_Algoritmi.pro` file.
When configuring the project, choose *Debug* version.
Finally, run the project in Debug mode.
Exit the application once it starts running, and necessary `.gcda` files should be generated inside the build directory.

Similarly, open *GATest* project inside Qt Creator, by opening its `GATest.pro` file, and build it in Debug mode.
This will create `GATest` executable inside the build directory.
